// OBJECTIVE:
// toggle power switch
// push button switch to turn LEDs D1-2 for 5s and D3 for 10s
// potentiometer to adjust brightness of D1-2 using PWM
// potentiometer to adjust blinking rate of LED D3 (5-10Hz)
// Have a heartbeat LED blinking at 1 HZ attached to pin D13

// DEBUGGING:
#include <Arduino.h>
// Variables, Macro, Function
#define LED_D1_PIN 5
#define LED_D2_PIN 6 // D5 and D6 are PWM-enabled
#define LED_D3_PIN 3 
#define LED_D4_PIN 13 // LED (D4) attached to pin D13
#define BUTTON_INTERRUPT_PIN 2 
#define ASTABLE_POT_PIN_1 A0
#define ASTABLE_POT_PIN_2 A1
// D3
#define ASTABLE_MIN_BLINK_RATE_HZ 5
#define ASTABLE_MAX_BLINK_RATE_HZ 10
#define ANALOG_READ_RANGE 1023 
#define LED_D3_DUTY_CYCLE_PCT 50
#define LED_D3_ON_DURATION_MS 10000
// D2 & D1
#define LED_D1_D2_ON_DURATION_MS 5000
// D4
#define LED_D4_BLINK_FREQ_HZ 1
#define LED_D4_DUTY_CYCLE_PCT 50
#define LED_D4_OFF_TIME_MS (LED_D4_DUTY_CYCLE_PCT*10)/LED_D4_BLINK_FREQ_HZ // 1HZ means one cycle per second: Off time = 500/1 = 0.5s
#define LED_D4_ON_TIME_MS ((100-LED_D4_DUTY_CYCLE_PCT)*10)/LED_D4_BLINK_FREQ_HZ 


// VARAIBLES
volatile bool illuminate_led = 0;
bool read_illumination_state = 1;
unsigned long led_on_time_start_ms;  
unsigned long current_time_ms;
unsigned long duration_timing;
// Read Voltage
unsigned long astable_pot_voltage_raw_1;
unsigned long astable_pot_voltage_raw_2;
// D3
unsigned long led_D3_frequency;
unsigned long led_D3_on_time_ms;
unsigned long led_D3_off_time_ms;
unsigned long led_D3_next_on_time_ms;
unsigned long led_D3_next_off_time_ms;
// D1 + D2
unsigned long led_D1_D2_voltage;
// D4
unsigned long led_D4_next_on_time_ms;
unsigned long led_D4_next_off_time_ms;


void do_this_after_button_press(void);
void read_astable_voltage_1(void);
void read_astable_voltage_2(void);
void led_D3_blink_frequency(void);
void led_D1_D2_brightness(void);
void leds_regulation(void);
void turn_led_on(uint8_t pin);
void turn_led_off(uint8_t pin);


void setup() {
    pinMode(LED_D1_PIN, OUTPUT);
    pinMode(LED_D2_PIN, OUTPUT);
    pinMode(LED_D3_PIN, OUTPUT);
    pinMode(LED_D4_PIN, OUTPUT);
    pinMode(ASTABLE_POT_PIN_1, OUTPUT);
    pinMode(ASTABLE_POT_PIN_2, OUTPUT);
    pinMode(BUTTON_INTERRUPT_PIN, INPUT_PULLUP); // pull pin up to HIGH if not driven by input signal
    
    attachInterrupt(digitalPinToInterrupt(BUTTON_INTERRUPT_PIN), do_this_after_button_press, FALLING);  // FALLING = HIGH -> LOW transition
    Serial.begin(9600);

    led_D3_next_on_time_ms = 0;
    led_D4_next_on_time_ms = 0;
}

void loop() {
    if (illuminate_led) {
      // if (read_illumination_state) {
      read_astable_voltage_1();
      led_D3_blink_frequency();

      read_astable_voltage_2();
      led_D1_D2_brightness();

      while (read_illumination_state){
        led_on_time_start_ms = millis(); 
        
        led_D3_next_on_time_ms = millis(); // 10377
        led_D3_next_off_time_ms = led_D3_next_on_time_ms + led_D3_on_time_ms; // 10477
        read_illumination_state = false;
      } // I want to read star_time only once

      leds_regulation();
    }
}

void do_this_after_button_press() {
    illuminate_led = 1;
}

void read_astable_voltage_1(){
  astable_pot_voltage_raw_1 = analogRead(ASTABLE_POT_PIN_1);
}

void read_astable_voltage_2(){
  astable_pot_voltage_raw_2 = analogRead(ASTABLE_POT_PIN_2);
}

void led_D3_blink_frequency(){
  led_D3_frequency = (ASTABLE_MIN_BLINK_RATE_HZ + (ASTABLE_MAX_BLINK_RATE_HZ-ASTABLE_MIN_BLINK_RATE_HZ)*(float)astable_pot_voltage_raw_1/ANALOG_READ_RANGE);
  led_D3_on_time_ms = (LED_D3_DUTY_CYCLE_PCT*10)/led_D3_frequency;
  led_D3_off_time_ms = led_D3_on_time_ms;
}

void led_D1_D2_brightness(){
  led_D1_D2_voltage = ((float)astable_pot_voltage_raw_2/ANALOG_READ_RANGE*255); // eg. 500/1023*255 = 0.5*255
  // Serial.println(astable_pot_voltage_raw_2);
  // Serial.println(led_D1_D2_voltage);
  analogWrite(LED_D1_PIN,led_D1_D2_voltage);
  analogWrite(LED_D2_PIN,led_D1_D2_voltage);
}

void leds_regulation(){
  duration_timing = millis(); ///////
  if ((duration_timing - led_on_time_start_ms) <= LED_D1_D2_ON_DURATION_MS){
    //D1 + D2, blinking for 5s, brightness adjusted by potentiometer 2
    turn_led_on(LED_D1_PIN);
    turn_led_on(LED_D2_PIN);
    }   

  else if ((duration_timing - led_on_time_start_ms) > LED_D1_D2_ON_DURATION_MS) {
    turn_led_off(LED_D1_PIN);
    turn_led_off(LED_D2_PIN);
    }
  
  if ((duration_timing - led_on_time_start_ms) <= LED_D3_ON_DURATION_MS){
    current_time_ms = millis();  // 20000
    Serial.println(current_time_ms);
    Serial.println(led_D3_next_on_time_ms);
    Serial.println(led_D3_next_off_time_ms);
    if ((current_time_ms > led_D3_next_on_time_ms) && (current_time_ms < led_D3_next_off_time_ms)){ ///// unsure about the codes here
        turn_led_on(LED_D3_PIN);
        Serial.println("led_D3_on");
    }

    if (current_time_ms >= led_D3_next_off_time_ms){
        turn_led_off(LED_D3_PIN);
        Serial.println("Led_D3_off");
        led_D3_next_on_time_ms = led_D3_next_off_time_ms + led_D3_off_time_ms; /// 20200
        led_D3_next_off_time_ms = led_D3_next_on_time_ms + led_D3_on_time_ms; /// 20300
    }
  }  

  else if ((duration_timing - led_on_time_start_ms) > LED_D1_D2_ON_DURATION_MS) {
    turn_led_off(LED_D3_PIN);
    }

  if (current_time_ms >= led_D4_next_on_time_ms){ /// current time
      turn_led_on(LED_D4_PIN);
      led_D4_next_off_time_ms = current_time_ms + LED_D4_ON_TIME_MS; 
      led_D4_next_on_time_ms = led_D4_next_off_time_ms + LED_D4_OFF_TIME_MS; 
      }

  else if (current_time_ms >= led_D4_next_off_time_ms){
      turn_led_off(LED_D4_PIN);
  }
}
  
void turn_led_on(uint8_t pin){
    digitalWrite(pin, 1);
}

void turn_led_off(uint8_t pin){
    digitalWrite(pin, 0);
}  

// Previous Codes:

// void led_D3_blink(){ 
//   current_time_ms = millis();
//   //// tasks: 1. start blinking from start 2. blink at a certain rate (adjustable) 3. stop blinking after 10s
//   if (current_time_ms >= led_D3_next_on_time_ms){
//       turn_led_on(LED_D3_PIN);
//       led_D3_next_off_time_ms = current_time_ms + led_D3_on_time_ms; 
//       led_D3_next_on_time_ms = led_D3_next_off_time_ms + led_D3_off_time_ms; 
//   }

//   if (current_time_ms >= led_D3_next_off_time_ms){
//       turn_led_off(LED_D3_PIN);
//   }
// }

// void led_D1_D2_on(){
//   turn_led_on(LED_D1_PIN);
//   turn_led_on(LED_D2_PIN);
// }

// void led_D4_blink(){
//     current_time_ms = millis(); 
//     if (current_time_ms >= led_D4_next_on_time_ms){
//         turn_led_on(LED_D4_PIN);
//         led_D4_next_off_time_ms = current_time_ms + LED_D4_ON_TIME_MS; 
//         led_D4_next_on_time_ms = led_D4_next_off_time_ms + LED_D4_OFF_TIME_MS; 
//         }

//     if (current_time_ms >= led_D4_next_off_time_ms){
//         turn_led_off(LED_D4_PIN);
//     }
// }
